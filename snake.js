//***************************************************//
var dir, MURS, SNAKETAB, FOOD_POS, grid_width, grid_height, speed, intervalID, niveau, audio;
const cell_size = 25;

const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");

const EMPTY = 0;
const FOOD = 1;
const SNAKE = 2;
const MUR = 3;
const COULEUR = ["#EEE", "#F00", "#0F0", "#111"];

var WORLD = [];
var score = 0;

let frame = false; //sert a ne pas changer de direction plusieurs fois en un seul affichage

const head_image = new Image();
head_image.src = './assets/tete.jpg';
const food_image = new Image();
food_image.src = './assets/nourriture.jpg';
const mur_image = new Image();
mur_image.src = './assets/mur.jpg';

//Init function
function init(){
	frame = false;

	//dessin du quadrillage
	ctx.fillStyle = '#000';
	for (let i = 0; i <= grid_width; i++) {
        ctx.fillRect(cell_size * i, 0, 1, canvas.height); // lignes verticales
    }
    for (let i = 0; i <= grid_height; i++) {
        ctx.fillRect(0, cell_size * i, canvas.width, 1); // lignes horizontales
    }
	//Remplissage des cases avec les couleurs associées
	for (let i = 0; i < WORLD.length; i++) {
        for (let j = 0; j < WORLD[i].length; j++) {
            ctx.fillStyle = COULEUR[WORLD[i][j]];
            ctx.fillRect(cell_size * j + 1, cell_size * i + 1, cell_size - 1, cell_size - 1);
        }
    }
    ctx.drawImage(head_image, SNAKETAB.slice(-1)[0][1] * cell_size + 1, SNAKETAB.slice(-1)[0][0] * cell_size + 1, cell_size - 1, cell_size - 1);
    ctx.drawImage(food_image, FOOD_POS[1] * cell_size + 1, FOOD_POS[0] * cell_size + 1, cell_size - 1, cell_size - 1);

    MURS.forEach((mur) => {
        ctx.drawImage(mur_image, mur[1] * cell_size + 1, mur[0] * cell_size + 1, cell_size - 1, cell_size - 1);
    });
}

function gameover() {
	alert("Score final : " + score);
	document.body.appendChild(menu);
	gameboard.remove();
}

function draw(){
    var snake_head = update_head();
    if (is_dead(snake_head)) { // vérification de la mort
        audio.pause();
        audio.currentTime = 0;
        clearInterval(intervalID);
        gameover();
    } else {
        if (!eat()) { // vérification du manger
            WORLD[SNAKETAB[0][0]][SNAKETAB[0][1]] = EMPTY;
            SNAKETAB.shift(); // enleve la queue uniquement si le serpent ne grandit pas
        }
        WORLD[snake_head[0]][snake_head[1]] = SNAKE;
        SNAKETAB.push(snake_head); // ajout de la nouvelle snake_head
    }
    init();
}

function update_head(){
	var old_head = SNAKETAB.slice(-1)[0];
    var new_head;
    // déplacement dans la bonne direction
    switch (dir) {
        case "RIGHT": //droite
            new_head = [old_head[0], old_head[1] + 1];
            break;
        case "DOWN": //bas
            new_head = [old_head[0] + 1, old_head[1]];
            break;
        case "LEFT": //gauche
            new_head = [old_head[0], old_head[1] - 1];
            break;
        case "UP": //haut
            new_head = [old_head[0] - 1, old_head[1]];
            break;
    }
    return new_head;
}

// mise à jour du score
function update_score() {
    score++;
    document.getElementById("score").innerText = score;
}

// manger la nourriture si possible
function eat() {
    var tete = SNAKETAB.slice(-1)[0];
    // On vérifie que la nourriture soit au même endroit que la tête
    if (tete[0] == FOOD_POS[0] && tete[1] == FOOD_POS[1]) {
        update_score();
        gen_food();
        return true;
    }
    return false;
}

// si la tete va sortir du cadre ou toucher une autre partie du corps
function is_dead(tete) {
    return (tete[0] < 0 || tete[1] < 0 || tete[0] >= grid_height || tete[1] >= grid_width) || WORLD[tete[0]][tete[1]] == SNAKE || (WORLD[tete[0]][tete[1]] == MUR);
}

function gen_food() {
    FOOD_POS[0] = Math.floor(Math.random() * grid_height);
    FOOD_POS[1] = Math.floor(Math.random() * grid_width);
    // Pour éviter que la nourriture apparaisse sur le serpent (les joies de l'aléatoire ^^)
    while (WORLD[FOOD_POS[0]][FOOD_POS[1]] !== EMPTY) {
        FOOD_POS[0] = Math.floor(Math.random() * grid_height);
        FOOD_POS[1] = Math.floor(Math.random() * grid_width);
    }
    WORLD[FOOD_POS[0]][FOOD_POS[1]] = FOOD;
}

function init_canvas(data) {
    grid_width = data.dimensions[0];
    grid_height = data.dimensions[1];
    speed = data.delay;
    FOOD_POS = data.food[0];
    SNAKETAB = data.snake;
    MURS = data.walls;
    score = 0;
    document.getElementById("score").innerText = score;
    canvas.width = grid_width * cell_size;
    canvas.height = grid_height * cell_size;
    dir = "RIGHT";

    // initialisation du tableau
    WORLD = [];
    for (let i = 0; i < grid_height; i++) {
        let temp = [];
        for (let j = 0; j < grid_width; j++) {
            temp.push(EMPTY);
        }
        WORLD.push(temp);
    }
    WORLD[FOOD_POS[0]][FOOD_POS[1]] = FOOD;

    // ajout des positions du serpent dans WORLD
    SNAKETAB.forEach((pos) => {
        WORLD[pos[0]][pos[1]] = SNAKE;
    });

    // ajout des positions des murs dans WORLD
    MURS.forEach((pos) => {
        WORLD[pos[0]][pos[1]] = MUR;
    });
    init();

	let radio = "./assets/su.mp3";
	if(document.getElementById('zic2').checked) { 
		radio = document.getElementById('zic2').value;
	}  
	else if(document.getElementById('zic3').checked) { 
		radio = document.getElementById('zic3').value;
	} 

	audio = new Audio(radio);
	audio.loop = true;
	audio.play();

    // démarrage de l'itération principale
    intervalID = setInterval(draw, speed);
}

function start_game() {
    document.body.appendChild(gameboard);
    niveau = menu.querySelector('input[name="niveau"]:checked').value;

	//getJson(niveau);

	var niveauUrl = "./levels/niveau" + niveau + ".json";
	//Parse json
    var req = new XMLHttpRequest();
    req.open("GET", niveauUrl);
    req.onerror = function () {
        console.log("Échec de chargement " + niveauUrl);
    };
    req.onload = function () {
        if (req.status === 200) {
            var data = JSON.parse(req.responseText);
            init_canvas(data);
			menu.remove();
        } else {
            console.log("Erreur " + req.status);
        }
    };
    req.send();

}

var gameboard = document.getElementById("gameboard");
var menu = document.getElementById("menu");
gameboard.remove();


document.addEventListener('keydown', function(event){

	if(event.keyCode == 37 && dir != 'RIGHT' && frame == false){
		dir = "LEFT";
		frame = true;
		
	}
	else if(event.keyCode == 38 && dir != 'DOWN' && frame == false){
		dir = "UP";
		frame = true;
	}
	else if(event.keyCode == 39 && dir != 'LEFT' && frame == false){
		dir = "RIGHT";
		frame = true;
		
	}
	else if(event.keyCode == 40 && dir != 'UP' && frame == false){
		dir = "DOWN";
		frame = true;
		
	}
});